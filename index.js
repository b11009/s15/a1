let x = parseInt(prompt("Enter your first number"));
let y = parseInt(prompt("Enter your second number"));
let name = prompt("Enter your name");
let age = parseInt(prompt("Enter your age"));

if (isNaN(x) || isNaN(y)) {
	console.warn("Please input numbers");
} else {

	let sumOfTwoNumbers = x + y;
		// console.log(sumOfTwoNumbers);
	let difference = x - y;
	let product = x * y;
	let quotient = x / y;

	if (sumOfTwoNumbers < 10) {
		 console.warn("The total of the two numbers is less than 10: " +sumOfTwoNumbers);
	}
	else if (sumOfTwoNumbers >= 10 && sumOfTwoNumbers <= 20) {
		console.log("The total of the two numbers is between 9 and 21: "+ sumOfTwoNumbers);
		alert("Difference: " + difference)
	}
	else if (sumOfTwoNumbers >= 21 && sumOfTwoNumbers <= 30) {
		console.log("The total of the two numbers is between 20 and 32: " + sumOfTwoNumbers);
		alert("Product: " + product);	
	}
	else if (sumOfTwoNumbers >= 31) {
		console.log("The total of the two numbers is greater than or equal to 31: " +sumOfTwoNumbers);
		alert("Quotient: " + quotient);	
	}
}

if ( name == "" || isNaN(age)) {
	alert("Are you a time traveler?");
} 
else {
	alert("Hello " + name +". Your age is " + age);
}

function isLegalAge(inputAge){
	try {
		inputAge.toString()
		if (inputAge >= 18) {
			alert("You are of legal age.");
		}
		else if (inputAge <= 17) {
			alert("You are not allowed here.");
		}
	}
	catch (error) {
		console.log(typeof error);
		console.warn(error.message);
		alert('Try catch finally trigger from isLegalAge function');	
	}
}

isLegalAge(age);

switch(age) {
	case 18:
		alert("You are now allowed to party.");
		break;
	case 21:
		alert("You are now part of the adult society.");
		break;
	case 65:
		alert("We thank you for your contribution to society.");
		break;
	default:
		alert("Are you sure you are not an alien?");
}

isLegalAge();

